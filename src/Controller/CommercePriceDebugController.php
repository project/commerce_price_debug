<?php

namespace Drupal\commerce_price_debug\Controller;

use Drupal\commerce\Context;
use Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginManager;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_store\SelectStoreTrait;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommercePriceDebugController.
 */
class CommercePriceDebugController extends ControllerBase {

  use SelectStoreTrait;

  /**
   * Debug manager.
   *
   * @var \Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginManager
   */
  protected $debugManager;

  /**
   * CommercePriceDebugController constructor.
   */
  public function __construct(CommercePriceDebugPluginManager $debug_manager) {
    $this->debugManager = $debug_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_price_debug.plugin.manager.price_debug_plugin')
    );
  }

  /**
   * Product debug page.
   */
  public function debugPageProduct(ProductInterface $commerce_product) {
    $variation = $commerce_product->getDefaultVariation();
    if (!$variation) {
      return ['#markup' => $this->t('@product_title does not have variations', ['@product_title' => $commerce_product->label()])];
    }
    $build = [];
    $build['content'] = [
      '#theme' => 'item_list',
      '#items' => [],
    ];
    $store = $this->selectStore($variation);
    $context = new Context($this->currentUser(), $store);
    foreach ($this->debugManager->getDefinitions() as $plugin_definition) {
      /** @var \Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginInterface  $plugin */
      $plugin = $this->debugManager->createInstance($plugin_definition['id']);
      if ($plugin) {
        $build['content']['#items'][$plugin_definition['id']] = [
          'title' => [
            '#type' => 'html_tag',
            '#tag' => 'strong',
            '#value' => $plugin->getDisplayLabel($variation) . ':'
          ],
          'debug' => $plugin->getDisplayDebugData($variation, $context),
        ];
      }
    }

    return $build;
  }

  /**
   * Build page title.
   */
  public function title(ProductInterface $commerce_product) {
    /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
    $variation = $commerce_product->getDefaultVariation();
    if (!$variation) {
      return $commerce_product->label();
    }

    return sprintf('%s(%s)', $variation->label(), $variation->getSku());
  }

}
