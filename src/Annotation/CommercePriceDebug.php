<?php

namespace Drupal\commerce_price_debug\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Commerce price debug item annotation object.
 *
 * @see \Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class CommercePriceDebug extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The label to be shown on the debug page.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $display_label;

}
