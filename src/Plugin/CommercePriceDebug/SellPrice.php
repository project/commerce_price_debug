<?php

namespace Drupal\commerce_price_debug\Plugin\CommercePriceDebug;

use Drupal\commerce\Context;
use Drupal\commerce_price\Entity\Currency;
use Drupal\commerce_price_debug\Annotation\CommercePriceDebug;
use Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginBase;
use Drupal\commerce_product\Entity\ProductVariationInterface;

/**
 * Product sell price.
 *
 * @CommercePriceDebug(
 *   id = "commerce_price_debug_sell_price",
 *   label = @Translation("Sell price"),
 * )
 */
class SellPrice extends CommercePriceDebugPluginBase {

  /**
   * {@inheritDoc}
   */
  public function getDisplayDebugData(ProductVariationInterface $variation, Context $context): array {
    /** @var \Drupal\commerce_price\Price $sell_price */
    $sell_price = $this->priceResolver->resolve($variation, 1, $context);
    if (!$sell_price) {
      return ['#markup' => $this->t('NULL')];
    }
    return [
      '#theme' => 'commerce_price_plain',
      '#number' => $sell_price->getNumber(),
      '#currency' => Currency::load($sell_price->getCurrencyCode()),
    ];
  }

}
