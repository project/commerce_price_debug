<?php

namespace Drupal\commerce_price_debug\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Commerce price debug plugin manager.
 */
class CommercePriceDebugPluginManager extends DefaultPluginManager {

  /**
   * Constructs a new CommercePriceDebugPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/CommercePriceDebug', $namespaces, $module_handler, 'Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginInterface', 'Drupal\commerce_price_debug\Annotation\CommercePriceDebug');

    $this->alterInfo('commerce_price_debug_plugin_info');
    $this->setCacheBackend($cache_backend, 'commerce_price_debug_commerce_price_debug_plugin_plugins');
  }

}
