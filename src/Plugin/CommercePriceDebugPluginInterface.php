<?php

namespace Drupal\commerce_price_debug\Plugin;

use Drupal\commerce\Context;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Commerce price debug plugins.
 */
interface CommercePriceDebugPluginInterface extends PluginInspectionInterface {

  /**
   * Get debug data.
   */
  public function getDisplayDebugData(ProductVariationInterface $variation, Context $context): array;

  /**
   * Get debug display label.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   Commerce variation entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Translatable label.
   */
  public function getDisplayLabel(ProductVariationInterface $variation);

}
