<?php

namespace Drupal\commerce_price_debug\Plugin;

use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\commerce_store\SelectStoreTrait;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Commerce price debug plugins.
 */
abstract class CommercePriceDebugPluginBase extends PluginBase implements CommercePriceDebugPluginInterface, ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Price resolver.
   *
   * @var \Drupal\commerce_price\Resolver\ChainPriceResolverInterface
   */
  protected $priceResolver;

  /**
   * CommercePriceDebugPluginBase constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ChainPriceResolverInterface $chain_price_resolver
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->priceResolver = $chain_price_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_price.chain_price_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplayLabel(ProductVariationInterface $variation): TranslatableMarkup {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['display_label'] ?? $plugin_definition['label'];
  }

}
