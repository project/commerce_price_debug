<?php

namespace Drupal\commerce_pricelist_debug\Plugin\CommercePriceDebug;

use Drupal\commerce\Context;
use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\commerce_price_debug\Annotation\CommercePriceDebug;
use Drupal\commerce_price_debug\Plugin\CommercePriceDebugPluginBase;
use Drupal\commerce_pricelist\PriceListPriceResolver;
use Drupal\commerce_pricelist\PriceListRepository;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Product sell price.
 *
 * @CommercePriceDebug(
 *   id = "commerce_pricelist_debug",
 *   label = @Translation("Pricelist price"),
 * )
 */
class CommercePricelistDebug extends CommercePriceDebugPluginBase {

  /**
   * Pricelist price resolver.
   *
   * @var \Drupal\commerce_pricelist\PriceListPriceResolver
   */
  protected $priceListPriceResolver;

  /**
   * Pricelist repository.
   *
   * @var \Drupal\commerce_pricelist\PriceListRepository
   */
  protected $priceListRepository;

  /**
   * CommercePricelistDebug constructor.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ChainPriceResolverInterface $chain_price_resolver,
    PriceListPriceResolver $pricelist_price_resolver,
    PriceListRepository $pricelist_repository
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $chain_price_resolver
    );
    $this->priceListPriceResolver = $pricelist_price_resolver;
    $this->priceListRepository = $pricelist_repository;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_price.chain_price_resolver'),
      $container->get('commerce_pricelist.price_resolver'),
      $container->get('commerce_pricelist.repository')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getDisplayDebugData(ProductVariationInterface $variation, Context $context): array {
    $pricelist_price = $this->priceListPriceResolver->resolve($variation, 1, $context);
    if ($pricelist_price) {
      $pricelist_item = $this->priceListRepository->loadItem($variation, 1, $context);
      return [
        '#type' => 'link',
        '#url' => $pricelist_item->toUrl('edit-form'),
        '#title' => $pricelist_item->label() . '(' . $pricelist_price->getNumber() . ')',
      ];
    }

    return ['#markup' => $this->t('NULL')];
  }

}
